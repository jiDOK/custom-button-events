﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class IngredientManager : MonoBehaviour
{
    [SerializeField]
    Button cacaoButton;
    [SerializeField]
    Button cinnamonButton;
    [SerializeField]
    Ingredient cacao;
    [SerializeField]
    Ingredient cinnamon;

    public IngredientEvent IngredientMessage = new IngredientEvent();

    void Start()
    {
        cacao = new Ingredient("cacao", 2);
        cinnamon = new Ingredient("cinnamon", 1);
        cacaoButton.onClick.AddListener(SetCacao);
        cinnamonButton.onClick.AddListener(SetCinnamon);
    }

    void SetCacao()
    {
        IngredientMessage.Invoke(cacao);
    }
    void SetCinnamon()
    {
        IngredientMessage.Invoke(cinnamon);
    }

}

// Boilerplate Code, verlangt Unity, um ein Event mit Custom Data zu bekommen
[System.Serializable]
public class IngredientEvent : UnityEvent<Ingredient>
{
}

// Serializable und SerializeField, um die Zutaten im Editor sichtbar zu machen
[System.Serializable]
public class Ingredient
{
    //private Backing-Variable und public Property
    [SerializeField] string name;
    public string Name { get { return name; } }

    [SerializeField] int amountPerClick;
    public int AmountPerClick
    {
        get { return amountPerClick; }
        set { amountPerClick = value; }
    }

    [SerializeField] int total = 0;
    public int Total
    {
        get { return total; }
        set { total = value; }
    }

    // Constructor
    public Ingredient(string name, int amountPerClick)
    {
        this.name = name;
        this.amountPerClick = amountPerClick;
    }
}