﻿using UnityEngine;

public class Beverage : MonoBehaviour
{
    Ingredient secretIngredient;

    public void SetIngredient(Ingredient ingredient)
    {
        secretIngredient = ingredient;
        secretIngredient.Total += secretIngredient.AmountPerClick;
        Debug.Log(secretIngredient.Name);
    }
}
